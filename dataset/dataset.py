from torch.utils.data import Dataset, random_split
import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize, RegexpTokenizer
from nltk.stem import WordNetLemmatizer
from gensim.models import KeyedVectors
from khmernltk import word_tokenize
import re
import numpy as np

from huggingface_hub import hf_hub_download
import fasttext


class KhmerSentimentTextDataset(Dataset):
    def __init__(self, csv_path, vocab) -> None:
        super().__init__()
        self.df = pd.read_csv(csv_path, encoding='utf-8')
        self.vocab = vocab
        self.stop_words = ["ទាំង", "ដ៏", "បើ", "ក៏", "ដែរ", "ទេ", "អ្នកណា", "អ្វី", "នៅឯណា", "ពេលណា", "ហេតុអ្វី", "យ៉ាងម៉េច", "ពេលខ្លះ", "មិនដែល", "ទីនេះ", "ទីនោះ", "គ្រប់ទីកន្លែង", "កន្លែងណាមួយ", "នៅទីណាមួយ", "ណាស់", "មែន", "មែនទែន", "ពិតប្រាកដ", "ឥឡូវនេះ", "ពេលក្រោយ", "យប់នេះ", "យប់ម្សិលមិញ", "ព្រឹកនេះ", "ម្សិលមិញ", "ថ្ងៃនេះ",
                           "ថ្ងៃស្អែក", "អាទិត្យក្រោយ", "រួចរាល់", "រួចហើយ", "នៅតែ", "និង", "ដែល", "ជា", "តែ", "ដើម្បី", "ប៉ុន្តែ", "ដោយសារ", "ពេលដែល", "ហើយ", "ដូចជា", "ដូច្នេះ", "ពេលណាមួយ", "ទៅវិញ", "តែម្តង", "ជាមួយ",  "ដូចគ្នានឹង", "រួចហើយទេ", "ឬ", "ដោយ", "ខ្ញុំ", "អ្នក", "នាង", "ពួក", "យើង", "ពួកគេ", "លោក", "គាត់", "គេ", "នេះ", "នោះ", "ណា"]
        self.regex_tokenizer = RegexpTokenizer(r'[a-zA-Z]+')
        self.lemmatizer = WordNetLemmatizer()

    def __len__(self) -> int:
        return len(self.df)

    def __getitem__(self, index: int):
        row = self.df.iloc[index]
        raw_text = row["translated_text"],
        text, label = row["translated_text"], row["sentiment_score"]
        text = self.preprocess(text)

        tokens = word_tokenize(text)
        token_idx = np.zeros(len(tokens))
        for (index, t) in enumerate(tokens):
            if t in self.vocab:
                token_idx[index] = self.vocab[t]
            else:
                token_idx[index] = -1

        return token_idx, label, raw_text

    def preprocess(self, translated_text):
        pattern = r'[។៘ៗ៕៚៙៖។...()+=»«-]'

        # Remove URLs
        translated_text = re.sub(r'http\S+', '', translated_text)

        # Remove special characters (excluding digits and numbers)
        translated_text = re.sub(r'[^ក-៹\s0-9]', '', translated_text)

        # Replace multiple spaces with a single space
        translated_text = re.sub(r'\s+', ' ', translated_text).strip()

        # Remove English letters and numbers
        translated_text = re.sub(r'[a-zA-Z0-9]', '', translated_text)

        # Remove English punctuation and special characters
        translated_text = re.sub(
            r'[!"#$%&\'()*+,-./:;<=>?@\[\\]^_`{|}~]', '', translated_text)

        # Replace multiple spaces with a single space
        translated_text = re.sub(r'\s+', ' ', translated_text).strip()

        # Remove Khmer special characters
        translated_text = re.sub(pattern, '', translated_text)

        # Remove specific Khmer words
        for word in self.stop_words:
            translated_text = translated_text.replace(word, '')

        # Remove space in between words
        translated_text = translated_text.replace(" ", "")

        return translated_text


if __name__ == "__main__":
    # embedding_model = KeyedVectors.load_word2vec_format("/Users/menghang/Desktop/ml-dev/ml-replicate/sentiment-analysis-v2/weights/GoogleNews-vectors-negative300.bin", binary=True)

    model_path = hf_hub_download(
        repo_id="facebook/fasttext-km-vectors", filename="model.bin")

    # Load the model
    fasttext_model = fasttext.load_model(model_path)

    vocab = {}
    for k, v in enumerate(fasttext_model.get_words()):
        vocab[v] = k

    dataset = KhmerSentimentTextDataset(
        "data/raw/mini_training/mini_training.csv",
        vocab=vocab
    )

    # Retrieve and print items from the dataset
    for i in range(len(dataset)):
        text, label = dataset[i]
        print(f"Item {i}: Text: {text}, Label: {label}")
