
import argparse
from models.sa_bilstm import SaBiLSTM


def evaluate(opt):
    model = SaBiLSTM
    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ckpt", default='',
                        help="model weight")
    parser.add_argument("--batch_size", default=32,
                        help="batch_size")
    parser.add_argument("--verbose", default=False, help="save predictions")
    opt = parser.parse_args()

    evaluate(opt)
