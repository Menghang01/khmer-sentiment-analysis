import pandas as pd 
import numpy as np 



def process(path):
    df = pd.read_csv(path, encoding='utf-8')
    random_rows = df.sample(n=32)
    random_rows.to_csv('data/raw/mini_training/mini_training_val.csv', index=False)

if __name__ == "__main__":
    path = "data/raw/sentiment_dataset.csv"
    process(path)