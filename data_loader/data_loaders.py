from typing import Any

import numpy as np
from torch.utils.data import DataLoader
from torch.utils.data.dataloader import default_collate
from torch.utils.data.sampler import SubsetRandomSampler
import torch
from huggingface_hub import hf_hub_download
import fasttext


class KhmerSentimentTextDataLoader(DataLoader):
    """
    Base class for all data loaders
    """

    def __init__(self, dataset, batch_size=128, shuffle=True, validation_split=0.1, num_workers=1, collate_fn=default_collate):
        self.validation_split = validation_split
        self.shuffle = shuffle
        self.dataset = dataset

        self.batch_idx = 0
        self.n_samples = len(dataset)
        self.num_workers = num_workers
        self.validation_split = 0.1

        self.sampler, self.valid_sampler = self._split_sampler(
            self.validation_split)

        self.init_kwargs = {
            'dataset': dataset,
            'batch_size': batch_size,
            'shuffle': self.shuffle,
            'collate_fn': collate_fn,
            'num_workers': num_workers
        }
        super().__init__(sampler=self.sampler, **self.init_kwargs)

    def _split_sampler(self, split):
        if split == 0.0:
            return None, None

        idx_full = np.arange(self.n_samples)

        np.random.seed(0)
        np.random.shuffle(idx_full)

        if isinstance(split, int):
            assert split > 0
            assert split < self.n_samples, "validation set size is configured to be larger than entire dataset."
            len_valid = split
        else:
            len_valid = int(self.n_samples * split)

        valid_idx = idx_full[0:len_valid]
        train_idx = np.delete(idx_full, np.arange(0, len_valid))

        train_sampler = SubsetRandomSampler(train_idx)
        valid_sampler = SubsetRandomSampler(valid_idx)

        self.shuffle = False
        self.n_samples = len(train_idx)

        return train_sampler, valid_sampler

    def split_validation(self):
        if self.valid_sampler is None:
            return None
        else:
            return DataLoader(sampler=self.valid_sampler, **self.init_kwargs)


class KhmerTextCollate(object):
    def __init__(self, model, vocab) -> None:
        self.model = model
        self.vocab = vocab

    def __call__(self, batch) -> Any:

        batch = filter(lambda x: x is not None, batch)

        sentences, labels, raw_text = zip(*batch)
        max_len = max(len(sentence) for sentence in sentences)

        embedding = np.zeros((len(sentences), max_len, 300))
        for i, sentence in enumerate(sentences):
            sentence_embedding = np.zeros((max_len, 300))
            for idx, token in enumerate(sentence):
                word = self.vocab[token]
                sentence_embedding[idx] = self.model.get_word_vector(word)
            embedding[i] = sentence_embedding

        return torch.Tensor(embedding), torch.LongTensor(labels), raw_text


if __name__ == "__main__":
    model_path = hf_hub_download(
        repo_id="facebook/fasttext-km-vectors", filename="model.bin")
    fasttext_model = fasttext.load_model(model_path)
    vocab_word_to_idx = {}
    vocab_idx_to_word = {}

    for k, v in enumerate(fasttext_model.get_words()):
        vocab_word_to_idx[v] = k
        vocab_idx_to_word[k] = v

    text_collate = KhmerTextCollate(fasttext_model, vocab_idx_to_word)
    data = [
        ([1, 2, 3], 0),  # (sentence, label)
        ([1, 2, 3, 4], 1),
        ([1, 2], 0),
    ]
    print(text_collate(data))
