import torch

from models.sa_bilstm import SaBiLSTM

from utils.common import prepare_device
import torch.nn as nn
import argparse

from huggingface_hub import hf_hub_download
import fasttext
from data_loader.data_loaders import KhmerSentimentTextDataLoader, KhmerTextCollate
from dataset.dataset import KhmerSentimentTextDataset
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def train(opt):

    model_path = hf_hub_download(
        repo_id="facebook/fasttext-km-vectors", filename="model.bin")
    fasttext_model = fasttext.load_model(model_path)

    vocab_word_to_idx = {}
    vocab_idx_to_word = {}
    vocab_idx_to_word[-1] = "UNK"

    for k, v in enumerate(fasttext_model.get_words()):
        vocab_word_to_idx[v] = k
        vocab_idx_to_word[k] = v

    dataset = KhmerSentimentTextDataset(
        opt.train_data,
        vocab=vocab_word_to_idx,
    )

    val_dataset = KhmerSentimentTextDataset(
        opt.valid_data,
        vocab=vocab_word_to_idx,
    )

    text_collate = KhmerTextCollate(fasttext_model, vocab_idx_to_word)

    train_data_loader = KhmerSentimentTextDataLoader(
        dataset=dataset, batch_size=opt.batch_size, shuffle=True, num_workers=0, collate_fn=text_collate)
    valid_data_loader = KhmerSentimentTextDataLoader(
        dataset=val_dataset, batch_size=opt.batch_size, shuffle=True, num_workers=0, collate_fn=text_collate)

    model = SaBiLSTM(
        input_size=300, hidden_size=128,
        output_size=3, num_layers=2,
    )

    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(
        model.parameters(), lr=0.0001,  weight_decay=1e-4)

    model = model.to(device)
    model.train()

    training_loss, validation_loss = 0, 0
    total_train_samples = 0

    for epoch in range(opt.epoch):
        for _, (text, label, raw_text) in enumerate(train_data_loader):
            text, label = text.to(device), label.to(device)
            logits = model(text)
            loss = criterion(logits, label)
            training_loss += loss.item() * text.size(0)
            total_train_samples += text.size(0)
            model.zero_grad()
            loss.backward()
            optimizer.step()

        avg_train_loss = training_loss / total_train_samples

        total_correct_predictions = 0
        total_valid_samples = 0
        validation_loss = 0

        with torch.no_grad():
            model.eval()

            for _,  (text, label, raw_text) in enumerate(valid_data_loader):

                text, label = text.to(device), label.to(device)
                logits = model(text)
                loss = criterion(logits, label)

                total_valid_samples += text.size(0)
                validation_loss += loss.item() * text.size(0)

                preds = torch.argmax(logits, dim=-1)
                correct_predictions = (preds == label).sum().item()

                total_correct_predictions += correct_predictions

            avg_valid_loss = validation_loss / total_valid_samples

            accuracy = total_correct_predictions / total_valid_samples * 100

        model.train()

        print(f"Epoch {epoch + 1}/{opt.epoch} - "
              f"Train Loss: {avg_train_loss:.4f}, "
              f"Validation Loss: {avg_valid_loss:.4f}, "
              f"Accuracy: {accuracy:.2f}%")

    PATH = "results/"

    torch.save(model.state_dict(), PATH + "sentiment-analysis.pth")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("--saved_model", default='',
                        help="continue training with saved weight")
    parser.add_argument(
        "--train_data", default="data/raw/mini_training/mini_training.csv", help="training data csv")
    parser.add_argument(
        "--valid_data", default="data/raw/mini_training/mini_training_val.csv", help="validation data csv")

    parser.add_argument("--epoch", default=40,
                        help="continue training with saved weight", type=int)

    parser.add_argument("--batch_size", default=32,
                        help="continue training with saved weight", type=int)

    opt = parser.parse_args()

    train(opt)
