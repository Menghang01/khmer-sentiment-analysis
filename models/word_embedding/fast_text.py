from huggingface_hub import hf_hub_download
import fasttext
import numpy as np 




class FastTextEmbedding(object):
    def __init__(self): 
        model_path = hf_hub_download(repo_id="facebook/fasttext-km-vectors", filename="model.bin")
        self.fasttext_model = fasttext.load_model(model_path)

    def __call__(self, token):
        try:
            return self.fasttext_model[token]
        except KeyError:
            return np.zeros(300)
            

     
