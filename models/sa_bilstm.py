import torch.nn as nn
from torch import zeros
from torch.autograd import Variable
import torch.nn.functional as F
import torch
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class SaBiLSTM(nn.Module):
    def __init__(self, input_size=300, hidden_size=512, output_size=3, num_layers=2, dropout=0.2,):
        super().__init__()
        self.lstm_layers = num_layers
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(input_size=input_size, hidden_size=hidden_size,
                            batch_first=True,  num_layers=num_layers, dropout=0.2,  bidirectional=True)
        self.fc1 = nn.Linear(2 * hidden_size, output_size)
        self.fc1 = nn.Linear(hidden_size * 2, hidden_size)
        self.fc2 = nn.Linear(hidden_size, 3)
        self.relu = nn.ReLU()
        self.dropout = nn.Dropout(dropout)

    def init_hidden(self, batch_size):
        h, c = (Variable(zeros(self.lstm_layers * 2, batch_size, self.hidden_size).to(device)),
                Variable(zeros(self.lstm_layers * 2, batch_size, self.hidden_size)).to(device))
        return h, c

    def forward(self, x):

        batch_size = x.shape[0]

        hidden_state, cell_state = self.init_hidden(batch_size)

        output,  _ = self.lstm(x, (hidden_state, cell_state))

        output = output[:, -1, :]

        rel = self.relu(output)
        dense1 = self.fc1(rel)
        drop = self.dropout(dense1)
        preds = self.fc2(drop)
        return preds
